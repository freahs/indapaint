package inda.paint.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import inda.paint.IndaPaint;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		int screenWidth = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		int screenHeight = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
		config.width = (int) (screenWidth * 0.75f);
		config.height = (int) (screenHeight*0.75f);
		config.resizable = false;
		config.vSyncEnabled = true;
		//config.foregroundFPS = 240;
		new LwjglApplication(new IndaPaint(), config);
	}
}