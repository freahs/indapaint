package inda.paint.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public abstract class AbstractTool implements Tool {

	private static final int MAX_SIZE = 30;
	private static final int MIN_SIZE = 1;

	private Color color;
	private int size;
	private String name;

	public AbstractTool(String name) {
		this.name = name;
	}

	@Override
	public void addToSize(int size) {
		int newSize = this.size + size;
		if (newSize >= MIN_SIZE && newSize <= MAX_SIZE)
			this.size = newSize;
	}

	protected int getSize() {
		return size;
	}

	protected void setSize(int size) {
		this.size = size;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	protected Color getColor() {
		return color;
	}

	@Override
	public String getName() {
		return name;
	}

	protected void drawCrosshair(ShapeRenderer shapeRenderer, float x, float y) {
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(getColor());
		shapeRenderer.line(x - 15, y, x - 5, y);
		shapeRenderer.line(x + 15, y, x + 5, y);
		shapeRenderer.line(x, y - 15, x, y - 5);
		shapeRenderer.line(x, y + 15, x, y + 5);
		shapeRenderer.line(x, y, x, y);
		shapeRenderer.end();
	}

}
