package inda.paint.tools;

import com.badlogic.gdx.graphics.Color;

public class BrushTool extends PaintingTool {

	public BrushTool() {
		super("brush");
	}

	@Override
	public void setColor(Color color) {
		super.setColor(new Color(color.r, color.g, color.b, 0.5f));
	}

}
