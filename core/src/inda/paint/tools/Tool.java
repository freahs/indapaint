package inda.paint.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public interface Tool {

	public void start(Pixmap pixmap, float x, float y);

	public void update(Pixmap pixmap, float x, float y);

	public void stop(Pixmap pixmap, float x, float y);

	public void setColor(Color color);

	public void addToSize(int size);

	public String getName();

	public void drawCursor(ShapeRenderer shape, float x, float y);

	public void draw(Batch batch);

	public boolean madeChanges();
}
