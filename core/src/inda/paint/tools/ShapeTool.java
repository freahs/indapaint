package inda.paint.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class ShapeTool extends PaintingTool {

	public enum Mode {
		RECT_FILLED("shape_square_filled"),
		CIRCLE_FILLED("shape_circle_filled"),
		RECT_LINE("shape_square"),
		CIRCLE_LINE("shape_circle"),
		LINE("shape_line");

		private final String name;

		private Mode(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}

	}

	private float startx, starty, stopx, stopy;
	private Mode mode;

	public ShapeTool(Mode mode) {
		super(mode.toString());
		this.mode = mode;
	}

	@Override
	public void start(Pixmap pixmap, float x, float y) {
		startx = x;
		starty = y;
		stopx = x;
		stopy = y;
		render();
	}

	@Override
	public void update(Pixmap pixmap, float x, float y) {
		stopx = x;
		stopy = y;
		render();
	}

	@Override
	public void stop(Pixmap pixmap, float x, float y) {
		stopx = x;
		stopy = y;
		addPixmap(pixmap);
		clear();
	}

	@Override
	public void drawCursor(ShapeRenderer shape, float x, float y) {
		drawCrosshair(shape, x, y);
	}

	private void render() {

		shapeBuffer.begin();
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		switch (mode) {
		case RECT_FILLED:
			drawRect(shapeRenderer, true);
			break;
		case RECT_LINE:
			drawRect(shapeRenderer, false);
			break;
		case CIRCLE_FILLED:
			drawCircle(shapeRenderer, true);
			break;
		case CIRCLE_LINE:
			drawCircle(shapeRenderer, false);
			break;
		case LINE:
			drawLine(shapeRenderer);
			break;
		}

		Gdx.gl.glDisable(GL20.GL_BLEND);
		shapeBuffer.end();
	}
	
	private void drawLine(ShapeRenderer shapeRenderer){
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.WHITE);
		shapeRenderer.rectLine(startx, starty, stopx, stopy, getSize());
		shapeRenderer.end();
	}

	private void drawRect(ShapeRenderer shapeRenderer, boolean filled) {
		float x = startx;
		float y = starty;
		if (startx > stopx)
			x = stopx;
		if (starty > stopy)
			y = stopy;
		float width = Math.abs(stopx - startx);
		float height = Math.abs(stopy - starty);

		shapeRenderer.begin(filled ? ShapeType.Filled : ShapeType.Line);
		shapeRenderer.setColor(Color.WHITE);
		shapeRenderer.rect(x, y, width, height);
		shapeRenderer.end();
	}

	private void drawCircle(ShapeRenderer shapeRenderer, boolean filled) {

		float width = Math.abs(stopx - startx);
		float height = Math.abs(stopy - starty);
		float radius = (width > height ? width : height) / 2f;
		float x = startx + radius / 2f;
		float y = starty + radius / 2f;

		if (!filled) {
			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.WHITE);
			for (int i = 0; i < getSize(); i++) {
				shapeRenderer.circle(x, y, radius - i / 2f);
			}
		} else {
			shapeRenderer.begin(ShapeType.Filled);
			shapeRenderer.setColor(Color.WHITE);
			shapeRenderer.circle(x, y, radius);
		}
		shapeRenderer.end();
	}

	private void clear() {
		shapeBuffer.begin();
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		shapeBuffer.end();
	}

}
