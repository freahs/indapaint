package inda.paint.tools;

import inda.paint.PixmapPool;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ScreenUtils;

abstract class PaintingTool extends AbstractTool {

	protected FrameBuffer shapeBuffer;
	protected ShapeRenderer shapeRenderer;
	private FrameBuffer pixmapBuffer;
	private SpriteBatch pixmapBatch;
	private TextureRegion region;
	protected final int width, height;

	private ArrayList<Vector2> coords;

	public PaintingTool(String name) {
		super(name);
		width = PixmapPool.instance().width;
		height = PixmapPool.instance().height;
		shapeBuffer = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		pixmapBuffer = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		coords = new ArrayList<Vector2>();
		shapeRenderer = new ShapeRenderer();
		pixmapBatch = new SpriteBatch();
		region = new TextureRegion(shapeBuffer.getColorBufferTexture());
		region.flip(false, true);
		setSize(3);
	}

	@Override
	public void start(Pixmap pixmap, float x, float y) {
		coords.add(new Vector2(x, y));
		render();
	}

	@Override
	public void update(Pixmap pixmap, float x, float y) {
		coords.add(new Vector2(x, y));
		render();
	}

	@Override
	public void stop(Pixmap pixmap, float x, float y) {
		addPixmap(pixmap);
		coords.clear();
		render();
	}

	@Override
	public boolean madeChanges() {
		return true;
	}

	@Override
	public void draw(Batch batch) {
		batch.setColor(getColor());
		batch.draw(region, 0, 0);
	}

	@Override
	public void drawCursor(ShapeRenderer shape, float x, float y) {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape.begin(ShapeType.Filled);
		shape.setColor(getColor());
		shape.circle(x, y, getSize() / 2f);
		shape.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
	}

	private void render() {
		shapeBuffer.begin();
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.WHITE);
		Vector2 prev = null;
		for (Vector2 v : coords) {
			shapeRenderer.circle(v.x, v.y, getSize() / 2f);
			if (prev != null)
				shapeRenderer.rectLine(prev, v, getSize());
			prev = v;
		}
		shapeRenderer.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
		shapeBuffer.end();
	}

	protected void addPixmap(Pixmap pixmap) {
		pixmapBuffer.begin();
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		pixmapBatch.begin();
		pixmapBatch.disableBlending();
		pixmapBatch.setColor(getColor());
		pixmapBatch.draw(shapeBuffer.getColorBufferTexture(), 0, height - Gdx.graphics.getHeight());
		pixmapBatch.end();
		Pixmap frameBufferPixmap = ScreenUtils.getFrameBufferPixmap(0, 0, width, height);
		pixmapBuffer.end();
		pixmap.drawPixmap(frameBufferPixmap, 0, 0);
		frameBufferPixmap.dispose();
	}
}
