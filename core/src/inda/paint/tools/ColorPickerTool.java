package inda.paint.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import inda.paint.canvas.Canvas;

public class ColorPickerTool extends UtilityTool {

	private Canvas canvas;

	public ColorPickerTool(Canvas canvas) {
		super("color_picker");
		this.canvas = canvas;
	}

	@Override
	public void update(Pixmap pixmap, float x, float y) {
	}

	@Override
	public void start(Pixmap pixmap, float x, float y) {
		y = pixmap.getHeight() - y;
		Color color = new Color(pixmap.getPixel((int) x, (int) y));
		color.a = 1f;
		canvas.setToolColor(color);
	}

	@Override
	public void stop(Pixmap pixmap, float x, float y) {

	}

	@Override
	public void drawCursor(ShapeRenderer shape, float x, float y) {
		drawCrosshair(shape, x, y);
	}

	@Override
	public boolean madeChanges() {
		return false;
	}

}
