package inda.paint.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public abstract class UtilityTool extends AbstractTool {

	private Color lineColor;

	public UtilityTool(String name) {
		super(name);
		lineColor = new Color(0.7f, 0.7f, 0.7f, 0.7f);
	}

	@Override
	public void draw(Batch batch) {
	}

	protected void drawDashedLine(ShapeRenderer shape, float from, float to, float fixed, boolean horizontal) {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape.begin(ShapeType.Line);
		shape.setColor(lineColor);

		float lineStep = 5;

		if (from > to) {
			float temp = to;
			to = from;
			from = temp;
		}

		while (from < to - lineStep) {
			if (horizontal)
				shape.line(from, fixed, from + lineStep, fixed);
			else
				shape.line(fixed, from, fixed, from + lineStep);
			from += 2 * lineStep;
		}

		if (to - from > lineStep) {
			if (horizontal)
				shape.line(from, fixed, to, fixed);
			else
				shape.line(fixed, from, fixed, to);
		}

		shape.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
	}

}
