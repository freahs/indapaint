package inda.paint.tools;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class SelectTool extends UtilityTool {

	private float startx, starty, stopx, stopy;
	private boolean active;

	public SelectTool() {
		super("select");
	}

	@Override
	public void start(Pixmap pixmap, float x, float y) {
		startx = x;
		starty = y;
		stopx = x;
		stopy = y;
		active = true;
	}

	@Override
	public void update(Pixmap pixmap, float x, float y) {
		stopx = x;
		stopy = y;
	}

	@Override
	public void stop(Pixmap pixmap, float x, float y) {
		stopx = x;
		stopy = y;

	}

	@Override
	public void drawCursor(ShapeRenderer shape, float x, float y) {
		drawCrosshair(shape, x, y);
		if (active) {
			drawDashedLine(shape, startx, stopx, stopy, true);
			drawDashedLine(shape, startx, stopx, starty, true);
			drawDashedLine(shape, starty, stopy, stopx, false);
			drawDashedLine(shape, starty, stopy, startx, false);
		}
	}

	@Override
	public boolean madeChanges() {
		return false;
	}

}
