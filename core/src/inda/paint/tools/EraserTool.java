package inda.paint.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class EraserTool extends AbstractTool {

	private Color eraseColor = new Color(0.2f, 0.2f, 0.2f, 0);

	public EraserTool() {
		super("eraser");
		setSize(5);
		super.setColor(new Color(0, 0, 0, 0.5f));
	}

	@Override
	public void setColor(Color color) {
	}

	@Override
	public void start(Pixmap pixmap, float x, float y) {
		y = pixmap.getHeight() - y;
		erase(pixmap, x, y);
	}

	@Override
	public void update(Pixmap pixmap, float x, float y) {
		y = pixmap.getHeight() - y;
		erase(pixmap, x, y);
	}

	private void erase(Pixmap pixmap, float x, float y) {

		float halfSize = getSize() / 2f;

		Color pixel = new Color();
		Pixmap.setBlending(Blending.None);
		for (int i = 0; i < getSize(); i++) {
			int px = (int) (x - halfSize + i);
			for (int j = 0; j < getSize(); j++) {
				int py = (int) (y - halfSize + j);
				Color.rgba8888ToColor(pixel, pixmap.getPixel(px, py));
				pixmap.drawPixel(px, py, Color.rgba8888(pixel.sub(eraseColor)));
			}
		}
		Pixmap.setBlending(Blending.SourceOver);

	}

	@Override
	public void stop(Pixmap pixmap, float x, float y) {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawCursor(ShapeRenderer shape, float x, float y) {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		shape.begin(ShapeType.Filled);
		shape.setColor(getColor());
		x = x - getSize() / 2f;
		y = y - getSize() / 2f;
		shape.rect(x, y, getSize(), getSize());
		shape.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);

	}

	@Override
	public void draw(Batch batch) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean madeChanges() {
		return true;
	}

}
