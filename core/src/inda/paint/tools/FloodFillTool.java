package inda.paint.tools;

import java.awt.Point;
import java.util.ArrayDeque;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

public class FloodFillTool extends UtilityTool {

	public FloodFillTool() {
		super("roller");
	}

	private void floodFill(Point n, Pixmap pixmap, int color) {
		if (!isTargetPixel(pixmap, n.x, n.y, color))
			return;
		ArrayDeque<Point> q = new ArrayDeque<Point>();
		q.add(n);
		while (!q.isEmpty()) {
			Point p = q.pollFirst();
			if (isTargetPixel(pixmap, p.x, p.y, color)) {
				Point w = new Point(p.x, p.y);
				Point e = new Point(p.x + 1, p.y);
				while (isTargetPixel(pixmap, w.x, w.y, color)) {
					setPixel(q, pixmap, w.x, w.y, color);
					w.x--;
				}
				while (isTargetPixel(pixmap, e.x, e.y, color)) {
					setPixel(q, pixmap, e.x, e.y, color);
					e.x++;
				}
			}
		}
	}

	private boolean isTargetPixel(Pixmap pixmap, int x, int y, int color) {
		if (x < 0 || y < 0)
			return false;
		if (x >= pixmap.getWidth() || y >= pixmap.getHeight())
			return false;
		if (pixmap.getPixel(x, y) != color)
			return false;
		if (color == Color.rgba8888(getColor()))
			return false;
		return true;
	}

	private void setPixel(ArrayDeque<Point> q, Pixmap pixmap, int x, int y, int color) {
		pixmap.drawPixel(x, y, Color.rgba8888(getColor()));
		if (isTargetPixel(pixmap, x, y - 1, color))
			q.addLast(new Point(x, y - 1));
		if (isTargetPixel(pixmap, x, y + 1, color))
			q.addLast(new Point(x, y + 1));
	}

	@Override
	public void start(Pixmap pixmap, float x, float y) {
	}

	@Override
	public void update(Pixmap pixmap, float x, float y) {
	}

	@Override
	public void stop(Pixmap pixmap, float x, float y) {
		y = pixmap.getHeight() - y;
		int px = MathUtils.round(x);
		int py = MathUtils.round(y);
		int color = pixmap.getPixel(px, py);
		Point p = new Point(px, py);
		floodFill(p, pixmap, color);
	}

	@Override
	public void drawCursor(ShapeRenderer shape, float x, float y) {
		drawCrosshair(shape, x, y);
	}

	@Override
	public boolean madeChanges() {
		return true;
	}

}
