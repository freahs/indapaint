package inda.paint.canvas;

import inda.paint.TextureLoader;
import inda.paint.tools.Tool;
import inda.paint.ui.mousemenu.MouseMenuActor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class MouseMenuBuilder {
	private Canvas canvas;

	public MouseMenuBuilder(Canvas canvas) {
		this.canvas = canvas;
	}

	public MouseMenuActor getCurrentColorItem() {
		TextureRegion region = TextureLoader.getRegion("color");

		return new MouseMenuActor(region) {

			{
				setColor(canvas.getToolColor());
			}

			@Override
			public void execute() {
			}

			@Override
			public void reset() {
				clearActions();
				setScale(1.0f);
				setPosition(-getWidth() / 2f, -getHeight() / 2f);
				setColor(canvas.getToolColor());
				setVisible(false);
			}

		};
	}

	public MouseMenuActor getColorItem(final Color color) {

		TextureRegion region = TextureLoader.getRegion("color");

		return new MouseMenuActor(region) {

			@Override
			public void execute() {
				canvas.setToolColor(color);
			}

			@Override
			public void reset() {
				clearActions();
				setScale(1.0f);
				setPosition(-getWidth() / 2f, -getHeight() / 2f);
				setColor(color);
				setVisible(false);
			}

		};
	}

	public MouseMenuActor getToolItem(final Tool tool) {

		TextureRegion region = TextureLoader.getRegion(tool.getName());

		return new MouseMenuActor(region) {

			@Override
			public void execute() {
				canvas.setTool(tool);
			}
		};
	}
}
