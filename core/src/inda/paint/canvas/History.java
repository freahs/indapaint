package inda.paint.canvas;

import inda.paint.PixmapPool;

import java.util.ArrayDeque;

import com.badlogic.gdx.graphics.Pixmap;

public class History {

	private static final int historyLength = 10;

	private PixmapStack historyBack;
	private PixmapStack historyForward;

	public History() {
		historyBack = new PixmapStack();
		historyForward = new PixmapStack();
	}

	public void update(Pixmap current) {
		// Pushes a copy of the current pixmap onto the undo stack.
		historyBack.push(PixmapPool.instance().copy(current));
		historyForward.clear();
	}

	public void undo(Pixmap current) {
		// Pops the most recent pixmap (i.e. the one to undo) in the historyBack
		// stack and pushes it onto the historyForward stack.
		historyForward.push(historyBack.pop());

		// Replaces the current pixmap with now topmost one in the historyBack
		// stack.
		PixmapPool.instance().update(current, historyBack.peek());
	}

	public void redo(Pixmap current) {
		// Replaces the current with its corresponding layer in the
		// historyForward stack;
		PixmapPool.instance().update(current, historyForward.peek());

		// Pops the most recent layer in the historyForward stack and pushes it
		// onto the historyBack stack.
		historyBack.push(historyForward.pop());
	}

	private class PixmapStack {
		private ArrayDeque<Pixmap> queue = new ArrayDeque<Pixmap>();

		void push(Pixmap layer) {
			if (queue.size() == historyLength)
				PixmapPool.instance().release(queue.removeLast());
			queue.push(layer);
		}

		Pixmap peek() {
			return queue.peek();
		}

		Pixmap pop() {
			return queue.pop();
		}

		void clear() {
			while (!queue.isEmpty()) {
				PixmapPool.instance().release(queue.remove());
			}
		}

		int size() {
			return queue.size();
		}
	}

	public boolean canUndo() {
		return historyBack.size() > 1;
	}

	public boolean canRedo() {
		return historyForward.size() > 0;
	}

}
