package inda.paint.canvas;

import inda.paint.PixmapPool;
import inda.paint.tools.Tool;
import inda.paint.ui.mousemenu.MouseMenuGroup;

import java.text.SimpleDateFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class Canvas extends Actor {
	private MouseMenuGroup mouseMenu;
	private ShapeRenderer shapeRenderer;

	private Pixmap pixmap;
	private Texture texture;
	private Tool tool;
	private Color toolColor;
	private History history;

	public Canvas(float width, float height) {
		shapeRenderer = new ShapeRenderer();
		texture = new Texture((int) width, (int) height, Format.RGBA8888);
		pixmap = PixmapPool.instance().newPixmap(false);
		history = new History();
		history.update(pixmap);
		toolColor = Color.WHITE;
		setupListeners();
		setSize(width, height);
		setOrigin(width / 2f, height / 2f);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.enableBlending();
		batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		batch.setColor(Color.WHITE);
		texture.draw(pixmap, 0, 0);
		batch.draw(texture, 0, 0);

		if (tool != null) {
			tool.draw(batch);
			batch.end();
			float x = Gdx.input.getX();
			float y = Gdx.graphics.getHeight() - Gdx.input.getY();
			tool.drawCursor(shapeRenderer, x, y);
			batch.begin();
		}
	}

	private void setupListeners() {
		addListener(new InputListener() {
			private boolean ctrl = false;

			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if (keycode == Input.Keys.CONTROL_LEFT) {
					ctrl = true;
					return true;
				}

				if (keycode == Input.Keys.Z && ctrl) {
					undo();
					return true;
				}

				if (keycode == Input.Keys.Y && ctrl) {
					redo();
					return true;
				}

				if (keycode == Input.Keys.S && ctrl) {
					save();
					return true;
				}

				return false;
			}

			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				if (keycode == Input.Keys.CONTROL_LEFT) {
					ctrl = false;
					return true;
				}

				if (ctrl && (keycode == Input.Keys.Z || keycode == Input.Keys.Y || keycode == Input.Keys.S))
					return true;

				return false;
			}

			@Override
			public boolean scrolled(InputEvent event, float x, float y, int amount) {
				if (tool != null)
					tool.addToSize(amount * (-1));

				return true;
			}

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (button == Input.Buttons.LEFT && !mouseMenu.isActive() && tool != null) {
					tool.start(pixmap, x, y);
					return true;
				}
				return false;
			}

			@Override
			public void touchDragged(InputEvent event, float x, float y, int pointer) {
				tool.update(pixmap, x, y);
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				tool.stop(pixmap, x, y);
				if (tool.madeChanges())
					history.update(pixmap);
			}
		});
	}

	public boolean canRedo() {
		return history.canRedo();
	}

	public boolean canUndo() {
		return history.canUndo();
	}

	public void save() {
		// TODO browser or named by date/time
		String fileName = "indapaint_";
		fileName += new SimpleDateFormat("yyyy-MM-dd_HHmmss_").format(System.currentTimeMillis());
		fileName += getWidth() + "x" + getHeight() + ".png";
		FileHandle file = Gdx.files.external(fileName);
		if (file.exists()) {
			// TODO dialog
			System.out.println("File already exists!");
		} else {
			PixmapIO.writePNG(file, pixmap);
			if (file.exists()) {
				System.out.println("File saved!");
			}
		}
	}

	public void undo() {
		history.undo(pixmap);
	}

	public void redo() {
		history.redo(pixmap);
	}

	public void restart() {
		PixmapPool.instance().clear(pixmap, false);
		history.update(pixmap);
	}

	public void setTool(Tool tool) {
		this.tool = tool;
		if (this.tool != null)
			this.tool.setColor(toolColor);
	}

	public Tool getTool() {
		return tool;
	}

	public void setToolColor(Color toolColor) {
		this.toolColor = toolColor;
		if (tool != null)
			tool.setColor(toolColor);
	}

	public Color getToolColor() {
		return toolColor;
	}

	public void setMouseMenu(MouseMenuGroup mouseMenu) {
		this.mouseMenu = mouseMenu;
	}
}
