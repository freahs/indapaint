package inda.paint;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureLoader {

	private static TextureAtlas atlas;

	private TextureLoader() {
	}

	public static void init(String path) {
		if (atlas != null) {
			throw new IllegalAccessError("TextureLoader can only be initialized once.");
		}
		atlas = new TextureAtlas(Gdx.files.internal(path));
	}

	public static TextureRegion getRegion(String name) {
		if (atlas == null) {
			throw new IllegalAccessError("TextureLoader must be initialized before usage.");
		}

		TextureRegion region = atlas.findRegion(name);

		if (region == null)
			throw new IllegalArgumentException("Region " + name + " not found.");

		return region;
	}

	public static void dispose() {
		atlas.dispose();
		atlas = null;
	}

}
