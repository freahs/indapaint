package inda.paint.ui.mousemenu;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MouseMenuGroup extends Group implements MouseMenuItem {

	private enum Status {
		WAITING, OPEN, CLOSED;
	}

	protected static final float focusDuration = 0.1f;
	protected static final float openDuration = 0.1f;
	protected static final float menuRadius = 70f;

	private Actor centerItem;
	private Actor switchItem;

	private Status status = Status.WAITING;

	private boolean canHaveCenterItem = true;
	private boolean canSwitchCenterItem = true;

	private boolean isActive;

	public MouseMenuGroup() {
		setupListener();
	}

	@Override
	public void addActor(Actor actor) {
		if (!(actor instanceof MouseMenuItem))
			throw new IllegalArgumentException("Actors in " + getClass().getSimpleName() + " must implement " + MouseMenuItem.class.getSimpleName());
		if (canHaveCenterItem && centerItem == null)
			centerItem = actor;
		super.addActor(actor);
	}

	@Override
	public void addActorAt(int index, Actor actor) {
		if (!(actor instanceof MouseMenuItem))
			throw new IllegalArgumentException("Actors in " + getClass().getSimpleName() + " must implement " + MouseMenuItem.class.getSimpleName());
		if (canHaveCenterItem && centerItem == null)
			centerItem = actor;
		super.addActorAt(index, actor);
	}

	@Override
	public void addActorBefore(Actor actorBefore, Actor actor) {
		if (!(actor instanceof MouseMenuItem))
			throw new IllegalArgumentException("Actors in " + getClass().getSimpleName() + " must implement " + MouseMenuItem.class.getSimpleName());
		if (canHaveCenterItem && centerItem == null)
			centerItem = actor;
		addActorBefore(actorBefore, actor);
	}

	@Override
	public void addActorAfter(Actor actorAfter, Actor actor) {
		if (!(actor instanceof MouseMenuItem))
			throw new IllegalArgumentException("Actors in " + getClass().getSimpleName() + " must implement " + MouseMenuItem.class.getSimpleName());
		if (canHaveCenterItem && centerItem == null)
			centerItem = actor;
		super.addActorAfter(actorAfter, actor);
	}

	protected void setupListener() {

		addListener(new ClickListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				Actor target = event.getTarget();

				if (status == Status.WAITING && target == centerItem)
					execute();
				else if (status == Status.OPEN && !isChild(target))
					close(target);

			}

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {

				Actor target = event.getTarget();

				if (button == Input.Buttons.RIGHT) {
					for (Actor a : getChildren())
						a.notify(event, false);
				}

				if (button == Input.Buttons.LEFT) {
					if (status == Status.OPEN && isChild(target)) {
						switchItem = target;
						((MouseMenuItem) target).execute();
						close(null);
					}
				}

				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

				Actor target = hit(x, y, true);

				if (button == Input.Buttons.RIGHT && isChild(target)) {
					switchItem = target;
					((MouseMenuItem) target).execute();
					close(null);
				}
			}

		});
	}

	@Override
	public void setVisible(boolean visible) {
		if (centerItem != null)
			centerItem.setVisible(visible);
	}

	@Override
	public void setTouchable(Touchable touchable) {
		if (centerItem != null)
			centerItem.setTouchable(touchable);
	}

	@Override
	public void close(Actor actor) {
		if (!getChildren().contains(actor, true)) {
			status = Status.CLOSED;
			for (Actor a : getChildren()) {
				((MouseMenuItem) a).close(actor);
			}
		}
	}

	@Override
	public void reset() {
		if (switchItem != null && canSwitchCenterItem) {
			centerItem = switchItem;
			switchItem = null;
		}
		setPosition(0, 0);
		status = Status.WAITING;
		for (Actor a : getChildren()) {
			((MouseMenuItem) a).reset();
		}
	}

	@Override
	public void execute() {
		status = Status.OPEN;
		float current = (float) (Math.PI / 2.0);
		float step = (float) (2 * Math.PI) / ((float) getChildren().size - (centerItem == null ? 0 : 1));
		for (Actor node : getChildren()) {
			float x = (float) Math.cos(current) * menuRadius;
			float y = (float) Math.sin(current) * menuRadius;
			if (node != centerItem) {
				node.setVisible(true);
				node.setTouchable(Touchable.disabled);
				node.addAction(Actions.sequence(
					Actions.moveBy(x, y, openDuration), 
					Actions.touchable(Touchable.enabled)
				));
				current += step;
			}
		}
	}

	private boolean isChild(Actor actor) {
		return getChildren().contains(actor, true);
	}

	public void setCenterItem(Actor actor) {
		canHaveCenterItem = true;
		canSwitchCenterItem = false;
		addActor(actor);
		centerItem = actor;
	}

	public void canHaveActive(boolean bool) {
		canHaveCenterItem = bool;
	}

	public void canSwitch(boolean bool) {
		canSwitchCenterItem = bool;
	}

	public void setActive(boolean active) {
		this.isActive = active;
	}

	public boolean isActive() {
		return isActive;
	}

	public void addChildren(Actor node, Actor... nodes) {
		addActor(node);

		for (Actor o : nodes) {
			addActor(o);
		}
	}

	public InputListener getTargetListener() {

		return new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (button == Input.Buttons.RIGHT) {
					MouseMenuGroup.this.notify(event, false);
					MouseMenuGroup.this.setActive(true);
					MouseMenuGroup.this.reset();
					MouseMenuGroup.this.setVisible(true);
					MouseMenuGroup.this.setPosition(x, y);
					MouseMenuGroup.this.execute();

				}
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

				if (button == Input.Buttons.RIGHT) {
					MouseMenuGroup.this.setActive(false);
					MouseMenuGroup.this.close(null);
				}
			}
		};
	}
}
