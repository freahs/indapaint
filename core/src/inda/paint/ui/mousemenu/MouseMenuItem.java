package inda.paint.ui.mousemenu;

import com.badlogic.gdx.scenes.scene2d.Actor;

public interface MouseMenuItem {

	public void close(Actor actor);

	public void reset();

	public void execute();
}
