package inda.paint.ui.mousemenu;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

/**
 * A item in a {@link MouseMenuGroup} which is responsible for executing
 * whatever actions that are associated with it.
 * 
 * @author Fredrik
 * @version beta
 *
 */
public abstract class MouseMenuActor extends Actor implements MouseMenuItem {

	static final float focusDuration = 0.1f;
	static final float closeDuration = 0.1f;

	private TextureRegion icon;
	// private TextureRegion icon_background;
	// private TextureRegion icon_gloss;

	private float iconWidth, iconHeight;
	private float iconOffsetX, iconOffsetY;

	public MouseMenuActor(TextureRegion textureRegion) {
		this.icon = textureRegion;
		// this.icon_background = TextureLoader.getRegion("icon_background");
		// this.icon_gloss = TextureLoader.getRegion("icon_gloss");
		textureRegion.getTexture()
				.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		setSize(icon.getRegionWidth(), icon.getRegionHeight());
		setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
		iconWidth = icon.getRegionWidth();
		iconHeight = icon.getRegionHeight();
		iconOffsetX = (getWidth() - iconWidth) / 2f;
		iconOffsetY = (getHeight() - iconHeight) / 2f;
		setVisible(false);
		setupListener();
	}

	private void setupListener() {

		addListener(new InputListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				addAction(Actions.scaleTo(1.3f, 1.3f, focusDuration));
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				addAction(Actions.scaleTo(1.0f, 1.0f, focusDuration));
			}

		});
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		// batch.setColor(new Color(0,0,0,0.5f));
		// batch.draw(icon_background, getX(), getY(), getOriginX(),
		// getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(),
		// getRotation());
		batch.setColor(getColor());
		batch.draw(icon, getX() + iconOffsetX, getY() + iconOffsetY, getOriginX(), getOriginY(), iconWidth, iconHeight, getScaleX(), getScaleY(), getRotation());
		// batch.setColor(new Color(1, 1, 1, 0.2f));
		// batch.draw(icon_gloss, getX(), getY(), getOriginX(), getOriginY(),
		// getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}

	@Override
	public void close(Actor actor) {
		if (actor == this)
			return;
		setTouchable(Touchable.disabled);
		addAction(Actions.sequence(
			Actions.parallel(
				Actions.scaleTo(0.5f, 0.5f, closeDuration),
				Actions.fadeOut(closeDuration)
			),
			Actions.visible(false)
		));
	}

	@Override
	public void reset() {
		clearActions();
		setScale(1.0f);
		setPosition(-getWidth() / 2f, -getHeight() / 2f);
		setColor(Color.WHITE);
		setVisible(false);
	}

}
