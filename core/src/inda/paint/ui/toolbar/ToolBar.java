package inda.paint.ui.toolbar;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.esotericsoftware.tablelayout.Cell;

/**
 * ToolBar - Top ToolBar holds a Bar which holds actors, preferably of type Button.
 * 
 * @author Fredrik Åhs & Linus Ljung
 *
 */
 public class ToolBar extends Table implements Disposable {
	private HorizontalGroup bar = new HorizontalGroup();
	private Texture texture;

	public ToolBar() {
		top();
		add(bar).expandX();
		
		// Format elements in bar
		bar.pad(16);
		bar.space(32);
		bar.center();
		
		// Create background image
		Pixmap pixmap = new Pixmap(1, 1, Format.RGB888);
		pixmap.setColor(Color.BLACK);
		pixmap.fill();
		texture = new Texture(pixmap);
		setBackground(new NinePatchDrawable(new NinePatch(texture)));
	}

	/**
	 * Add button to HorizontalGroup
	 */
	@Override
	public Cell<?> add(Actor actor) {
        if(actor instanceof HorizontalGroup) {
        	// Only add actors of type HorizontalGroup to ToolBar
        	return super.add(actor);
        } else {
        	// Add all other actors to bar (HorizontalGroup)
            bar.addActor(actor);     
            return null;
        }
	}

	@Override
	public void dispose() {
		texture.dispose();
	}

}
