package inda.paint.ui.toolbar;

import inda.paint.TextureLoader;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * ToolBarButton - Buttons to go in ToolBar
 * 
 * @author Fredrik Åhs & Linus Ljung
 *
 */
public class ToolBarButton extends Button {
	private ButtonStyle bsEnabled = new ButtonStyle();
	private ButtonStyle bsDisabled = new ButtonStyle();

	/**
	 * @param atlas
	 */
	public ToolBarButton(String name) {
		setName(name);
		// Set textures for different modes
		bsEnabled.up = new TextureRegionDrawable(TextureLoader.getRegion(name + "-up"));
		bsEnabled.down = new TextureRegionDrawable(TextureLoader.getRegion(name + "-down"));
		bsEnabled.over = new TextureRegionDrawable(TextureLoader.getRegion(name + "-over"));
		bsDisabled.up = new TextureRegionDrawable(TextureLoader.getRegion(name + "-disabled"));
		setStyle(bsEnabled);
		
		/*
		 * Call execute, only of button is changed
		 */
		addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				execute();
			}
		});
	}

	/**
	 * @see com.badlogic.gdx.scenes.scene2d.ui.Button#setDisabled(boolean)
	 * Disabled button and change its texture to indicate
	 */
	@Override
	public void setDisabled(boolean isDisabled) {
		if (isDisabled) {
			setStyle(bsDisabled);
		} else {
			setStyle(bsEnabled);
		}
		super.setDisabled(isDisabled);
	}
	
	/**
	 * To be overridden for adding commands to button
	 */
	public void execute() {
	}

}
