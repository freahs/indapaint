package inda.paint;

import java.util.ArrayDeque;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

public class PixmapPool {

	public final int width;
	public final int height;
	private ArrayDeque<Pixmap> pixmaps;
	private Color backgroundColor = Color.BLACK;
	private static PixmapPool instance;

	public static void init(int width, int height) {
		if (instance != null) {
			throw new IllegalAccessError("PixmapPool can only be initialized once.");
		}
		instance = new PixmapPool(width, height);
	}

	public static PixmapPool instance() {
		if (instance == null) {
			throw new IllegalAccessError("PixmapPool must be initialized before usage.");
		}
		return instance;
	}

	private PixmapPool(int width, int height) {
		this.width = width;
		this.height = height;
		pixmaps = new ArrayDeque<Pixmap>();
	}

	private Pixmap getRawLayer() {
		Pixmap pixmap = pixmaps.poll();
		if (pixmap == null)
			pixmap = new Pixmap(width, height, Format.RGBA8888);
		return pixmap;
	}

	public Pixmap newPixmap(boolean alpha) {
		Pixmap pixmap = getRawLayer();
		return clear(pixmap, alpha);
	}

	public void release(Pixmap pixmap) {
		pixmaps.offer(pixmap);
	}

	public void update(Pixmap target, Pixmap source) {
		Pixmap.setBlending(Pixmap.Blending.None);
		target.drawPixmap(source, 0, 0);
		Pixmap.setBlending(Pixmap.Blending.SourceOver);
	}

	public Pixmap copy(Pixmap pixmap) {
		Pixmap copy = getRawLayer();
		Pixmap.setBlending(Pixmap.Blending.None);
		copy.drawPixmap(pixmap, 0, 0);
		Pixmap.setBlending(Pixmap.Blending.SourceOver);
		return copy;
	}

	public Pixmap clear(Pixmap pixmap, boolean alpha) {
		Pixmap.setBlending(Pixmap.Blending.None);
		pixmap.setColor(alpha ? Color.CLEAR : backgroundColor);
		pixmap.fill();
		Pixmap.setBlending(Pixmap.Blending.SourceOver);
		return pixmap;
	}

	public static void dispose() {
		while (!instance().pixmaps.isEmpty())
			instance().pixmaps.pop().dispose();
		instance = null;
	}

}
