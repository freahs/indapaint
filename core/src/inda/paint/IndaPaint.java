package inda.paint;

import inda.paint.canvas.Canvas;
import inda.paint.canvas.MouseMenuBuilder;
import inda.paint.splash.SplashImage;
import inda.paint.tools.BrushTool;
import inda.paint.tools.ColorPickerTool;
import inda.paint.tools.EraserTool;
import inda.paint.tools.FloodFillTool;
import inda.paint.tools.PencilTool;
import inda.paint.tools.ShapeTool;
import inda.paint.ui.mousemenu.MouseMenuActor;
import inda.paint.ui.mousemenu.MouseMenuGroup;
import inda.paint.ui.toolbar.ToolBar;
import inda.paint.ui.toolbar.ToolBarButton;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class IndaPaint extends ApplicationAdapter {
	public final static int TOOL_BAR_HEIGHT = 64;
	private Stage stage;
	private Canvas canvas;

	public void create() {
		stage = new Stage(new ScreenViewport());
		int width = (int) stage.getWidth();
		int height = (int) (stage.getHeight() - TOOL_BAR_HEIGHT);
		TextureLoader.init("data/indapaint_icons.pack");
		PixmapPool.init(width, height);
		canvas = new Canvas(width, height);
		canvas.setName("background canvas");
		stage.addActor(canvas);
		stage.setScrollFocus(canvas);
		setupToolBar(stage);
		setupMouseMenu(stage, canvas);

		SplashImage splash = new SplashImage("data/introsplash.png", canvas);
		stage.addActor(splash);
		stage.setKeyboardFocus(splash);

		Gdx.input.setInputProcessor(stage);
	}

	public void render() {
		Gdx.graphics.setTitle("FPS: " + Gdx.graphics.getFramesPerSecond());
		Gdx.graphics.getGL20().glClearColor(0, 0, 0, 1);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}

	public void dispose() {
		stage.dispose();
		PixmapPool.dispose();
		TextureLoader.dispose();
	}

	private void setupToolBar(Stage stage) {
		ToolBar toolbar = new ToolBar();
		toolbar.setWidth(stage.getWidth());
		toolbar.setHeight(TOOL_BAR_HEIGHT);
		toolbar.setY(stage.getHeight() - TOOL_BAR_HEIGHT);

		toolbar.add(new ToolBarButton("download") {
			@Override
			public void execute() {
				canvas.save();
			}
		});

		toolbar.add(new ToolBarButton("undo") {
			@Override
			public void execute() {
				canvas.undo();
			}

			@Override
			public void act(float delta) {
				setDisabled(!canvas.canUndo());
				super.act(delta);
			}
		});

		toolbar.add(new ToolBarButton("redo") {
			@Override
			public void execute() {
				canvas.redo();
			}

			@Override
			public void act(float delta) {
				setDisabled(!canvas.canRedo());
				super.act(delta);
			}
		});

		toolbar.add(new ToolBarButton("restart") {
			@Override
			public void execute() {
				canvas.restart();
			}
		});

		stage.addActor(toolbar);
	}

	private void setupMouseMenu(Stage stage, Canvas canvas) {
		MouseMenuBuilder menuBuilder = new MouseMenuBuilder(canvas);

		MouseMenuGroup root = new MouseMenuGroup();
		root.canHaveActive(false);
		root.canSwitch(false);

		MouseMenuActor eraser = menuBuilder.getToolItem(new EraserTool());
		MouseMenuActor pencil = menuBuilder.getToolItem(new PencilTool());
		MouseMenuActor brush = menuBuilder.getToolItem(new BrushTool());
		MouseMenuActor floodFill = menuBuilder.getToolItem(new FloodFillTool());
		MouseMenuActor picker = menuBuilder.getToolItem(new ColorPickerTool(canvas));

		MouseMenuGroup shapeGroup = new MouseMenuGroup();
		shapeGroup.addActor(menuBuilder.getToolItem(new ShapeTool(ShapeTool.Mode.RECT_FILLED)));
		shapeGroup.addActor(menuBuilder.getToolItem(new ShapeTool(ShapeTool.Mode.RECT_LINE)));
		shapeGroup.addActor(menuBuilder.getToolItem(new ShapeTool(ShapeTool.Mode.CIRCLE_FILLED)));
		shapeGroup.addActor(menuBuilder.getToolItem(new ShapeTool(ShapeTool.Mode.CIRCLE_LINE)));
		shapeGroup.addActor(menuBuilder.getToolItem(new ShapeTool(ShapeTool.Mode.LINE)));

		MouseMenuGroup colorGroup = new MouseMenuGroup();
		colorGroup.setCenterItem(menuBuilder.getCurrentColorItem());
		colorGroup.addActor(menuBuilder.getColorItem(Color.WHITE));
		colorGroup.addActor(menuBuilder.getColorItem(Color.BLACK));
		colorGroup.addActor(menuBuilder.getColorItem(Color.BLUE));
		colorGroup.addActor(menuBuilder.getColorItem(Color.CYAN));
		colorGroup.addActor(menuBuilder.getColorItem(Color.DARK_GRAY));
		colorGroup.addActor(menuBuilder.getColorItem(Color.GRAY));
		colorGroup.addActor(menuBuilder.getColorItem(Color.GREEN));
		colorGroup.addActor(menuBuilder.getColorItem(Color.LIGHT_GRAY));
		colorGroup.addActor(menuBuilder.getColorItem(Color.MAGENTA));
		colorGroup.addActor(menuBuilder.getColorItem(Color.ORANGE));
		colorGroup.addActor(menuBuilder.getColorItem(Color.PINK));
		colorGroup.addActor(menuBuilder.getColorItem(Color.RED));

		root.addChildren(colorGroup, pencil, brush, floodFill, picker, eraser, shapeGroup);

		canvas.addListener(root.getTargetListener());
		stage.addActor(root);
		canvas.setMouseMenu(root);
	}
}
