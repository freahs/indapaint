package inda.paint.splash;

import inda.paint.canvas.Canvas;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Scaling;

/**
 * SplashImage draws an bitmap stretching it's parent.
 * Dispose bitmap and remove itself from parent on any 
 * key our mouse click.
 * 
 * @author Fredrik Åhs & Linus Ljung
 *
 */
public class SplashImage extends Image implements Disposable{
	private Texture texture;
	private Canvas canvas;

	/**
	 * @param path internal path to bitmap image
	 * @param canvas needed for releasing kbd focus
	 */
	public SplashImage(String path, Canvas canvas) {
		this.canvas = canvas;
		this.texture = new Texture(Gdx.files.internal(path));
		setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
		
		// Format SplashImage
		setScaling(Scaling.none);
		setAlign(Align.top);
		
		// Need to fill parent after screen size change
		// instead of setSize(getPrefWidth(), getPrefHeight());
		setFillParent(true);

		addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				dispose();
				return;
			}

			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				return true;
			}

			@Override
			public boolean keyUp(InputEvent event, int keycode) {
				dispose();
				return true;
			}
		});
	}

	/**
	 * Dispose bitmap, remove image from stage and set kbd focus to canvas.
	 */
	@Override
	public void dispose() {
		getStage().setKeyboardFocus(canvas);
		remove();
		texture.dispose();
	}

}
